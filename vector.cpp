#include <iostream>
#include <vector>

// One of the drawbacks of the previous approach is that
// we only detect different dimensions during running time.
// Template parameters can be used to enlist the compiler 
// to help us detect these errors.
template <typename T = double, size_t DIMENSION = 3>
struct MathVector{
   std::vector<T> v;

   // Default constr. size of vector is now template param!
   MathVector( /* int n */ )   
      : v( std::vector<T>( DIMENSION, T() ) ) { } // DIMENSION doubles with default value  

   // Only construct math vectors with DIMENSION entries
   // Note: This needs to be documented.
   MathVector( std::vector<T> regularVector ) : v( regularVector ) { 
      if ( DIMENSION < regularVector.size() ) {
         std::cerr << "Dim. of expl. param > requested dim.: SLICING!\n";
      }
      else if ( DIMENSION > regularVector.size() ) {
         std::cerr << "Dim. of expl. param > requested dim.: PADDING!\n";
      }
      else{ }
      v.resize( DIMENSION , T() );
   } 
};


// Math operations
//    Addition ( operator+ )
template <typename T = double, size_t DIMENSION = 3>
MathVector<T,DIMENSION> operator+( MathVector<T,DIMENSION> lhsClone, 
                                   const MathVector<T,DIMENSION>& rhs ){ 

   for ( size_t i = 0 ; i < DIMENSION ; i++ )
      lhsClone.v[i] += rhs.v[i];
   
   return lhsClone;
}


// Math operations
//    Reflection ( unary operator- )
template <typename T = double, size_t DIMENSION = 3>
MathVector<T,DIMENSION> operator-( MathVector<T,DIMENSION> rhsClone ){
   for ( auto& x : rhsClone.v ){
      x = -x;
   }
   return rhsClone;
}


// Math operations
//    Subtraction ( binary operator- )
template <typename T = double, size_t DIMENSION = 3>
MathVector<T,DIMENSION> operator-( MathVector<T,DIMENSION> lhsClone, 
                                   const MathVector<T,DIMENSION>& rhs ){ 
   return lhsClone + (-rhs);  // <-- No longer a logic error!!!
}

// Math operations
//    Dot Product ( operator+ )
template <typename T = double, size_t DIMENSION = 3>
T operator*( const MathVector<T,DIMENSION>& lhs, 
             const MathVector<T,DIMENSION>& rhs ){ 
   T sum = 0;  // Assumes numeric type!
   for ( size_t i = 0 ; i <  lhs.v.size() ; ++i )
      sum += lhs.v[i] * rhs.v[i];

   return sum;
}


// Output
template <typename T = double, size_t DIMENSION = 3> 
std::ostream& operator<<( std::ostream& out, const MathVector<T,DIMENSION>& v ){
   for ( auto& x : v.v ){  // <-- Not a smiley ;-) ... but rather a poor parameter name choice
      out << x << " ";
   }
   return out;
}

template <typename T>
std::ostream& operator<<( std::ostream& out, const std::vector<T>& v ){
   for ( auto& x : v ){  
      out << x << " ";
   }
   return out;
}


int main(){
   using std::cout;
   using std::vector;

   vector<double> v = {1, 2, 3};

   // v.pop_back();             // <-- It doesn't bite any more.

   MathVector<> mv1 = v;


   // MathVector oR2(2);        // <-- Nope! Size is now a template param
   MathVector<double,2> oR2;
   MathVector<double> oR3;


   cout << "Regular vector (v): " << v << "\n";  
   //                             ^^^^^^^
   // It now works b/c operator<< is now overloaded to handle
   // regular vectors. The conversion below still works.
   // cout << "Regular vector (v): " << static_cast< MathVector<> >(v) << "\n";  

   cout << "Origin in R^2 (oR2): " << oR2 << "\n";
   cout << "Origin in R^3 (oR3): " << oR3 << "\n";
   cout << "Math vector 1 (mv1): " << mv1 << "\n";

   cout << "\n";
   cout << "Minus math vector 1 (-mv1): " << -mv1 << "\n";
   cout << "Origin minus mv1 (oR3-mv1): " << oR3 - mv1 << "\n";

   cout << "\n";
   cout << "Math vector plus itself (mv1+mv1): " << mv1 + mv1 << "\n";

   // Broken!!! That's OK, though. Regular vectors are not meant te be added.
   // cout << "Math vector plus regular vector (mv1+v): " << mv1 + v << "\n";
   // cout << "Regular vector plus regular vector (v+v): " << v + v << "\n";


   // Other operations...
   cout << "Dot product ( mv1*mv1 ): " << mv1 * mv1 << "\n";

   // Different dimensions?!
   // cout << "Different dimensions (mv1+oR2): " << mv1 + oR2 << "\n";
   // cout << "Dot product ( mv1*oR2 ): " << mv1 * oR2 << "\n";

   return 0;
}
