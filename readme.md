## Templates (revisited)

We take a trip down memory lane to study _templates_.

The goal(s) are to use templates to 

   * produce a similar effect than that achived by 
     using function pointers (callbacks).
   * enhance classes by providing flexibility
     in member functions. (E.g. custom sorting)
   * enlist the help of the compiler to detect
     incompatible objects.
